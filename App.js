import React ,{ useState } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, Alert, Platform  } from 'react-native';
import * as ImagePicker from 'expo-image-picker'
import * as Sharing from 'expo-sharing'
import uploadAnonymousFileAsync from 'anonymous-files'

export default function App() {

 const [selectedImage, setSelectedImage] = useState(null)

  let openImagePickerAsync = async () => {

  let permissionResult =  await ImagePicker.requestMediaLibraryPermissionsAsync()

    if (permissionResult.granted === false){

         alert('El permiso para tu camara es requerido');
         return;
    }

   const pickerResult = await ImagePicker.launchImageLibraryAsync()
  
   if (pickerResult.cancelled === true){
       return;

   }

  if(Platform.OS === 'web'){

       const remoteUri = await uploadAnonymousFileAsync(pickerResult.uri)
       setSelectedImage({localUri : pickerResult.uri, remoteUri})
 
  }else {
   setSelectedImage({localUri:  pickerResult.uri})
    }
  }


  const openShareDialog = async () => {
      if (!(await Sharing.isAvailableAsync())){
        
        alert(`The image is available for sharing at: ${selectedImage.remoteUri}`);
          return;

      }
    await Sharing.shareAsync(selectedImage.localUri);
  };

  return (
    <View style={styles.container}>

   <Text style={styles.title}>
      SELECT IMAGE
  </Text>

      <TouchableOpacity
      onPress={openImagePickerAsync}
      >
      <Image
        source={{uri: 
          selectedImage !== null 
        ? selectedImage.localUri 
        : 'https://picsum.photos/200/200',
        }}

        style={styles.image}
      />
        </TouchableOpacity> 


    {selectedImage ? (

      <TouchableOpacity
        onPress={openShareDialog}
        style={styles.button}
      >
        <Text style={styles.buttonText}>
         SHARE IN SOCIAL MEDIA
        </Text>
      </TouchableOpacity>
      ) :  (
        <View />

      )}     

 </View>
  );
};

const styles = StyleSheet.create({
  
  container:{ backgroundColor: "cyan", 
              flex: 1, 
              justifyContent: 'center', 
              alignItems: 'center' 
  },
  
  
  title: { fontSize: 30,
           color: "#000000"
  },
  
  
  image: { height: 200, 
           width: 200,
           borderRadius: 100,
           resizeMode: 'contain'
  },
  

  button: { backgroundColor: 'white',
            padding: 7,
            marginTop: 10,
            borderRadius: 100           
  },
  

  buttonText: { color: "red",
                fontSize: 20
  }
  

})
